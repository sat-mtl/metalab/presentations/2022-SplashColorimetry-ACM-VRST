---
title: Colorimetry Evaluation for Video Mapping Rendering
author: Eva Décorps, Christian Frisson, Emmanuel Durand
institute: Société des Arts Technologiques [SAT]
separator: <!--s-->
verticalSeparator: <!--v-->
theme: white 
revealOptions:
  transition: 'none' 
  loop: true
  slideNumber: true
  autoPlayMedia: true
--- 

<!-- .slide: id="header" -->
  <!-- <img src="images/logos/" style="height:70px;" />   -->
  <h2 data-i18n="title">Colorimetry Evaluation for Video Mapping Rendering</h2>
  <h4 data-i18n="conference">28th ACM Symposium on Virtual Reality Software and Technology (VRST 2022)</h4>
  <img src="images/logos/VRST2022_logo.png" style="height:70px;" />  

  <small><a href="https://gitlab.com/sat-mtl/metalab/tools/splash-colorimetry-evaluation">https://gitlab.com/sat-mtl/metalab/tools/splash-colorimetry-evaluation</a></small>

  <small>Eva Décorps, Christian Frisson, Emmanuel Durand</small>

[[SAT](https://sat.qc.ca/)] [metalab](https://sat-mtl.gitlab.io/metalab/)

<!--s-->
  <!-- .slide: id="sat" -->
  <h2 data-i18n="title"><a href="https://sat.qc.ca">Society for Arts and Technology</a></h2>
  <img src="images/sat.svg" style="height:700px;margin-top:-50px" />  
